$(document).ready(function(){


// ===========  CAMERA FUNCTION  ==============

    // Grab elements, create settings, etc.
    var video = document.getElementById('video');

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }

 
    function countDown() {
        var divs = $('.countdown');
        var timer
        var offset = 0 

        divs.each(function(){
            var self = this;
            timer = setTimeout(function(){
                $(self).css('font-size', '150px');
                $(self).fadeOut(1000);
            }, 1000 + offset);
            offset += 1000;
        });

        // flash
        setTimeout(function(){
            $('.flash').fadeIn(200);
            $('.flash').fadeOut(400);
            $(divs).css('font-size', '0px');
        }, 4000);
    }


    // function gifPhotos() {
    //     var divs = $('.countdown');
    //     var offset = 1500

    //     for ( i=0; i<5; i++) {

    //         (function(i){
    //           setTimeout(function(){
    //                 $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='640' height='480'></canvas></div>")
    //                 var canvas = $('.canvas');
    //                 var context = canvas[canvas.length -1].getContext('2d');
    //                 var video = document.getElementById('video');

    //                 $('.flash').fadeIn(200);
    //                 $('.flash').fadeOut(400);

    //                 $(divs).css('font-size', '0px');
    //                context.drawImage(video, 0, 0, 640, 480);
    //                $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');

    //                // watermark
    //                 // var img2 = new Image();
    //                 // img2.src = '/content/frontend/assets/img/logo_holder.png';
    //                 // context.drawImage(img2,530,420, 80, 43);

    //                 $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))
              
    //           }, offset);
    //           offset += 1500
    //         }(i));  
    //     }
    // }


    // function showGifs() {
    //     var photos = $('#pictures').children('.canvas_holder');
    //     $('#pictures').fadeIn(1000);
    //     $('.gif_info').fadeIn();
    //     for ( i=0; i<6; i++ ) {
    //         $(photos[i]).css('display', 'inline-block');
    //     }
    // }


    function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }

    function convertCanvasToImage2(canvas) {
        var dataURL = canvas.toDataURL();
        return canvas.src = dataURL;
    }

    


    function showPictures() {
        $('#pictures').fadeIn(2000);
        var photos = $('#pictures').children('.photo');
            $(photos[0]).fadeIn();
            // var p = convertCanvasToImage($(photos[0]))
            // console.log('hi' + p)

        //$($(photos[0]).html()).attr('src');
        $.post('/contact/save_img/',{image:$($(photos[0]).html()).attr('src')},function(data){
            $('#submit_form input[name=image]').val(data.image.id);
        });

    }



    // // GIF CLICK
    // document.getElementById("snap_gif").addEventListener("click", function() {
    //     $(this).fadeOut(1000)
    //     $(this).css('pointer-events', 'none');
    //     var pics = []

    //     countDown();
    //     setTimeout(function(){
    //         // for testing
    //         $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='640' height='480'></canvas></div>")
    //         var canvas = $('.canvas');
    //         var context = canvas[canvas.length -1].getContext('2d');
    //         var video = document.getElementById('video');

    //        context.drawImage(video, 0, 0, 640, 480);
    //        $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');

    //        // watermark
    //         // var img2 = new Image();
    //         // img2.src = '/content/frontend/assets/img/logo_holder.png';
    //         // context.drawImage(img2,530,420, 80, 43);

    //         $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))

    //        gifPhotos();

    //     }, 4000 )

    //     setTimeout(function(){
    //         $('#photos').css('opacity', '0');
    //         setTimeout(function(){
    //             $('#photos').fadeOut();
    //         }, 1100);

    //         showGifs();
    //         $(this).css('pointer-events', 'all');
    //     }, 10000);
    // });



    // // PHOTO CLICK
    document.getElementById("snap_photo").addEventListener("click", function() {
        $(this).fadeOut(1000);
        $(this).css('pointer-events', 'none');
        var pics = []

        countDown();
            // take the picture
            setTimeout(function(){
                $('#pictures').append("<div class='canvas_holder photo'><canvas class='canvas' width='1080' height='810'></canvas></div>")
                
                var canvas = $('.canvas');
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                // context.filter = 'invert(100%)'

               context.drawImage(video, 0, 0, 1080, 810);

               // watermark
                // var logo = new Image();
                // logo.src = '/content/frontend/assets/img/logo_holder.png';
                // context.drawImage(logo,530,420, 80, 43);

                $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))



            }, 4000);

        setTimeout(function(){
            $('.camera').fadeOut();
            $('.submit').delay(1000).fadeIn();
            $(this).css('pointer-events', 'all');
            showPictures();
        }, 5000);
    });

});

