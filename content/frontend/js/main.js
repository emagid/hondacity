$(document).ready(function(){

	$('.choice').click(function(){
    $(this).children('div').children('h2').children('span').css('padding-left', '50px');
  });

    $('.click_action').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            $('.choices').css('padding-top', '200px');
            $('.home').fadeOut(500); 
        }, 500);
        setTimeout(function(){
            window.location.href = link;
        }, 1000);
    });

    $('iframe').load(function(){
	  $(this).contents().find("body").on('click', function(event) { alert('test'); });
	});


    // form submit
    $('#contact_form').on('submit', function(e){
        e.preventDefault();
        $.post('/contact', $(this).serialize(), function (response) {
            console.log(response);

            $('#donation_alert').slideDown();
            $('#donation_alert').css('display', 'flex');

            setTimeout(function(){

                // $('#donation_alert').slideUp();
                window.location.href = '/';
            }, 3200);
        });
    });


    // Open Camera
    $('#open_cam').click(function(e){
      e.preventDefault();
      $('.home .overlay').fadeOut(1000);
      setTimeout(function(){
        $('.camera').fadeIn();
        $('.pic_text').fadeIn();
        $('#video').fadeIn();
        $('.camera').css('display', 'flex');
        $('#snap_photo').fadeIn();
        $('.camera').css('background-color', 'rgba(6, 33, 58, .8)');
      }, 1000);
    });



    // Share click
     $(document).on('click', '.submit', function(){
        $('.share_overlay').slideDown();
     });



     // Form submit
     $('#submit_form').on('submit', function(e){
        e.preventDefault();
        $.post('/contact', $(this).serialize(), function (response) {
            console.log(response);
            $('#submit_form')[0].reset();
            $('#share_alert').slideDown();
            $('#share_alert').css('display', 'flex');

            setTimeout(function(){
                $('#share_alert').slideUp();
                 window.location.href = '/';
            }, 3000);
        });
     });


     // Click of done sharing
     $('#done').on('click', function(e){
        $('.share_overlay').slideUp();
        $('#jQKeyboardContainer').remove()
        // remove all images
        $('#pictures').children('.canvas_holder');
     });


     // click of showing images
     $('.event_images').on('click', function(){
        $('.choice').fadeOut();
        $('#event_gifs').fadeIn();
        $('.display_images').slideDown();
     });







     // initial timeout redirect homepage
      // var initial = null;

      // function invoke() {
      //     initial = window.setTimeout(
      //         function() {
      //             window.location.href = '/';
      //         }, 60000);
      // }

      // invoke();

      // $('body').on('click mousemove', function(){
      //     window.clearTimeout(initial);
      //     invoke();
      // });

      // $('iframe').on('click mousemove', function(){
      //     window.clearTimeout(initial);
      //     invoke();
      // });


      // No Right clicks
      document.addEventListener('contextmenu', event => event.preventDefault());

    
});



