<?php

class registerController extends siteController
{
    function __construct()
    {
        parent::__construct();

    }

    /*public function index_post(Array $params = [])
    {
        $user = \Model\User::loadFromPost();
        $name = explode(" ", trim($_POST['name']));
        if(count($name) != 2){
            $n = new \Notification\ErrorHandler('Full name please.');
            $_SESSION["notification"] = serialize($n);
            redirect(isset($_POST['redirect_url']) && $_POST['redirect_url']!='/register'?$_POST['redirect_url']:'/');
        }

        $user->first_name = $name[0];
        $user->last_name = $name[1];
        $hash = \Emagid\Core\Membership::hash($user->password);
        $user->password = $hash['password'];
        $user->hash = $hash['salt'];
        if($user->save()){
            $customerRole = new \Model\User_Roles();
            $customerRole->role_id = 2;
            $customerRole->user_id = $user->id;
            $customerRole->save();

            $n = new \Notification\MessageHandler('Welcome to Diamond Lab Grown! Your account was successfully created');
            $_SESSION["notification"] = serialize($n);

            $email = new \Emagid\Email();
            $email->addTo($user->email);
            $email->subject('Welcome, '.$user->full_name().'!');
            $email->body = '<p><a href="www.americangrowndiamond.com"><img src="http://americangrowndiamonds.com/content/frontend/assets/img/logo_new.png" /></a></p>'
                .'<p><b>Dear '.$user->full_name().'</b></p>'
                .'<p>Welcome to <a href="www.americangrowndiamond.com">americangrowndiamond.com</a>. To log in when visiting our site just click My Account at the top of every page, and then enter your e-mail address and password.</p>'
                .'<p>When you log in to your account, you will be able to do the following:</p>'
                .'<ul>'
                .'<li>Proceed through checkout faster when making a purchase</li>'
                .'<li>Check the status of orders</li>'
                .'<li>View past orders</li>'
                .'<li>Make changes to your account information</li>'
                .'<li>Change your password</li>'
                .'<li>Store alternative addresses (for shipping to multiple family members and friends!)</li>'
                .'</ul>'
                .'<p>If you have any questions about your account or any other matter, please feel free to contact us at info@emagidCheckin.com or by phone at <a href="tel:18008535590">1.800.853.5590</a>.</p>'
                .'<p>Thanks again!</p>';
            $email->send();

            \Model\User::login($user->email,$_POST['password']);
        } else {
            $n = new \Notification\ErrorHandler($user->errors);
            $_SESSION["notification"] = serialize($n);
        };

        redirect(isset($_POST['redirect_url']) && $_POST['redirect_url']!='/register'?$_POST['redirect_url']:'/');
    }*/
    public function index(Array $params = [])
{
    $this->loadView($this->viewData);
}
    public function index_post(Array $params = [])
    {

        $user = \Model\User::loadFromPost($_POST);

        if($user->save()){
            $n = new \Notification\MessageHandler('We received your Submission.');
            $_SESSION["notification"] = serialize($n);
        }else{
            $n = new \Notification\ErrorHandler($user->errors);
            $_SESSION['notification'] = serialize($n);
        }
        redirect(SITE_URL);
    }
}