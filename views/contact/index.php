<main>

    <header>
        <a href="/"><img src="<?=FRONT_ASSETS?>img/honda_logo.png"></a>
        <a class='home_btn' href="/"><img src="<?=FRONT_ASSETS?>img/home.jpg"></a>
    </header>

    <section class='home contact_form'>
      <div class='overlay'>
        <form id='contact_form'>
          <input type="text" name="name" placeholder='Full Name'>
          <input type="email" name="email" placeholder='Email'>
          <input type="number" name="phone" placeholder='Phone Number'>
          <textarea type='text' name='message' placeholder='Comments'></textarea> 
          <input class='button' type="submit">
        </form>
      </div>
    </section>

    <section id='donation_alert'>
      <h3>Thank you!</h3>
      <p>We'll be in touch soon!</p>
    </section>

</main>



