<div id="page-wrapper">  
<!-- 	<div class='latest_media'>
		<img src="https://static.wixstatic.com/media/c11316_9ccc15c96fd04386b552cadf789f303f~mv2.gif">
	</div> -->
</div>

<script>

	window.location.href = "/admin/contacts"
	var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
	var dataSet = {"previous":{"2014-04":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-05":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-06":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-07":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-08":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-09":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-10":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"}},"current":{"2015-04":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-05":{"monthly_sales":"4078.22","gross_margin":0,"monthly_orders":"8"},"2015-06":{"monthly_sales":"9485.6","gross_margin":0,"monthly_orders":"25"},"2015-07":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-08":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-09":{"monthly_sales":"35","gross_margin":0,"monthly_orders":"1"},"2015-10":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"}}};

	var months = [];
	for (key in dataSet.previous){
		var month = new Date(key.split('-')[0], key.split('-')[1]-1);
		months.push(monthNames[month.getMonth()]);
	}

	var set = [[],[]];
	for (key in dataSet.previous){
		set[0].push(dataSet.previous[key].monthly_sales);
	}
	for (key in dataSet.current){
		set[1].push(dataSet.current[key].monthly_sales);
	}
	var barChartData1 = {
		labels : months,
		datasets : [
			{
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data : set[0]
			},
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : set[1]
			}
		]

	}

	var lineChartData = {
		labels : months,
		datasets : [
			{
				label: "Previous",
				fillColor : "rgba(220,220,220,0.2)",
				strokeColor : "rgba(220,220,220,1)",
				pointColor : "rgba(220,220,220,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(220,220,220,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			},
			{
				label: "Current",
				fillColor : "rgba(151,187,205,0.2)",
				strokeColor : "rgba(151,187,205,1)",
				pointColor : "rgba(151,187,205,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(151,187,205,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			}
		]

	}

	set = [[],[]];
	for (key in dataSet.previous){
		set[0].push(dataSet.previous[key].monthly_orders);
	}
	for (key in dataSet.current){
		set[1].push(dataSet.current[key].monthly_orders);
	}
	var barChartData2 = {
		labels : months,
		datasets : [
			{
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data : set[0]
			},
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : set[1]
			}
		]

	}

	window.onload = function(){
		var ctxB = document.getElementById("B").getContext("2d");
	 	window.myLine = new Chart(ctxB).Line(lineChartData, {
	 		responsive: true
	 	});
		var ctxA = document.getElementById("A").getContext("2d");
		window.myBar = new Chart(ctxA).Bar(barChartData1, {
			responsive : true
		});
		var ctxC = document.getElementById("C").getContext("2d");
		window.myBar = new Chart(ctxC).Bar(barChartData2, {
			responsive : true
		});
	}

</script> 
 
<?php echo footer(); ?>