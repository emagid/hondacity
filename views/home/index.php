<main>

    <header>
        <a href="/"><img src="<?=FRONT_ASSETS?>img/honda_logo.png"></a>
        <a class='home_btn' href="/"><img src="<?=FRONT_ASSETS?>img/home.jpg"></a>
    </header>

    <secition class='home'>
        <div class='overlay'>
            <div class='choices'>
                <a class='click_action choice' href="payments"><div>
                    <h2>BROWSE VEHICLES <span class='point'>></span></h2>
                </div>
                <a class='click_action choice' href="/deals"><div>
                    <h2>PAYMENT CALCUALTOR  <span>></span></h2>
                </div></a>
                <a class='choice'><div id='open_cam'>
                    <h2>TAKE A PHOTO  <span>></span></h2>
                </div></a>
                <!-- <a class='click_action' href="/service"><div>
                    <h2>SERVICE <span>></span></h2>
                </div></a>
                <a class='click_action' href="/contact"><div>
                    <h2>CONTACT <span>></span></h2>
                </div></a> -->
            </div>
        </div>

        <div class='camera'> 
            <h3 class='pic_text'>Click the camera below to take a picture</h3>
            <h3 class='gif_text'>Click the camera below to take a GIF</h3>           
            <video id="video" width="1080px" height="1690px" autoplay></video>
            <device type="media" onchange="update(this.data)"></device>
            <script>
                function update(stream) {
                    document.querySelector('video').src = stream.url;
                }
            </script>

                <div id='snap_photo' class='camera_button'>                 
                    <img src="<?=FRONT_ASSETS?>img/camera_w.png">
                </div>
                <div id='snap_gif' class='camera_button'>                   
                    <img src="<?=FRONT_ASSETS?>img/camera_w.png">
                </div>

                <!-- countdown --><div class='countdown'>3</div>
                <!-- countdown --><div class='countdown'>2</div>
                <!-- countdown --><div class='countdown'>1</div>
                <!-- flash --><div class='flash'></div>
        </div>

        <div id="pictures">
                <div class='button next'>NEXT</div>
                <p class='gif_info'>Choose 4 photos to create your GIF</p>
                <div class='button submit'>SHARE</div>
                <div class='container gif_container'></div>
        </div>


        <!-- Share overlay -->
            <section class='share_overlay'>
                <div class='container'>
                    <form id='submit_form'>
                        <input type='hidden' name='form' value="1">
                        <input type="hidden" name="image" class="image_encoded">
                        <input type="text" class='input' name="name" placeholder="Full Name">
                        <input required class='input' name='email' pattern="[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$" type='text' placeholder='Email' title='Please enter a valid email address.'>
                        <input id='share_btn' class='button reverse' type='submit' value='SHARE'>
                    </form>
                    <!-- <a href='/' id='done' class='button'>DONE</a> -->
                </div>
            </section>

            <!-- Alerts -->
            <section id='share_alert'>
                <h3>Thank you!</h3>
                <p>We'll send you an email shortly!</p>
            </section>

    </section>


</main>